---
author: BenDup
title: Chapitre 1 - très basique
---

## I. Introduction

Il faut maîtriser au minimum les opérations de maths, le `print` et le `input`.
![](images/serpent-teint.jpg){ width=30%; : .center }
<table>
<tr>
    <td style="border:1px solid; font-weight:bold;">X+Y</td>
    <td style="border:1px solid; font-weight:bold;">X-Y</td>
    <td style="border:1px solid; font-weight:bold;">X*Y</td>
    <td style="border:1px solid; font-weight:bold;">X**Y</td>
    <td style="border:1px solid; font-weight:bold;">X/Y</td>
    <td style="border:1px solid; font-weight:bold;">X//Y</td>
    <td style="border:1px solid; font-weight:bold;">X%Y</td>
</tr>
<tr>
    <td style="border:1px solid; font-weight:bold;">Addition de X à Y</td>
    <td style="border:1px solid; font-weight:bold;">Soustraction de X de Y</td>
    <td style="border:1px solid; font-weight:bold;">Multiplication de X par Y</td>
    <td style="border:1px solid; font-weight:bold;">X à la puissance Y</td>
    <td style="border:1px solid; font-weight:bold;">Quotient de la division de X par Y</td>
    <td style="border:1px solid; font-weight:bold;">Quotient de la division euclidienne (entier) de X par Y</td>
    <td style="border:1px solid; font-weight:bold;">Reste de la division euclidienne de X par Y (modulo)</td>
</tr>
</table>

### 1. Commençons en douceur

Afficher un message de bienvenue comme "hello", grâce à l'encart IDE.

???+ question "Modifier le message de bienvenue"

    Compléter le script ci-dessous :
    Afficher un message plus complexe comme « Hello je suis "Toto" au lycée »


    {{IDE('scripts/print_hello')}}


??? success "Astuce à dérouler"
    Il suffit de modifier le texte entre les guillemets.
    <br>
    Néanmoins si on veut afficher des guillemets dans les guillemets il faut utiliser ALT 174-175 ou \"      

### 2. Ajoutons une opération

Utilisons des variables a et b dans lesquelles nous stockerons des valeurs.
Puis afficher une phrase du style "La multiplication de a par b vaut : [`reponse`]"

Compléter et tester dans l'encart IDE précédent :
```python
a=3
b=6
print("La multiplication ...") # Compléter cette ligne
```       

??? success "Astuce à dérouler"
    Dans le `print`, le texte et les variables sont séparés par une virgule.
    On peut soit créer une variable c = a x b, soit faire le a x b dans le `print`.

## II. Incrustons une entrée interactive

Une entrée interactive fait apparaître une boîte de dialogue demandant à l'utilisateur une donnée de son choix. Cette donnée est stockée dans une variable et peut être utilisée par la suite.

### 1. Entrée puis affichage

L'entrée interactive s'appelle `input`. Demander à l'utilisateur son prénom, puis l'afficher.

Compléter et tester dans l'encart IDE précédent :
```python
prenom=input("Quel est votre prénom ?")
print("Votre prénom est ...") # Compléter cette ligne
```    

### 2. Entrée, opération(s) puis affichage

Demander d'entréer l'année de naissance de l'utilisateur, puis afficher son âge.

Compléter et tester dans l'encart IDE précédent :
!!! warning inline end "Important"
    Prenez le temps de lire les commentaires !
    Cliquez sur les +
```python
anneenaiss=int(input("Quel est votre année de naissance ?")) # (1)
age= # (2)
print("Vous avez ... ans") # Compléter cette ligne sans vous planter entre les guillemets et les virgules
```    

1. :warning: La mention `int()` est **obligatoire** autour d'un `input` dans lequel on demande une valeur qu'on traitera comme un nombre entier, sinon python considère que c'est juste une chaîne de caractères et refuse d'effectuer des opérations mathématiques avec !

2. Il faut créer une variable supplémentaire pour y stocker la différence entre l'année actuelle et l'année de naissance.

??? note pliée "Note à cliquer"

    Si à la place d'un entier je m'attendais à un nombre à virgule, il ne faudrait pas mettre `int()` mais `float()`.  

??? tip "Aller plus loin"

    Il existe bien d'autres fonctions comme : <br>
    - round(nombre,i) qui arrondit le nombre à i chiffres après la virgule ; <br>
    - abs(nombre) qui donne la valeur absolue d'un nombre ; <br>
    - sqrt(nombre) qui donne la racine carrée d'un nombre. Cette commande fait partie de la bibliothèque **math**, il faut donc penser à charger cette bibliothèque en début de programme, en tapant `from math import *` ; <br>
    - etc.  


