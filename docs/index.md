# Site pour l'enseignement interactif - Cité Curie - BD 2023

😊 Ce site permet l'entraînement intégré à <a href="https://www.python.org/" target="_blank">Python</a>. Cela signifie que vous pouvez exécuter des bouts de code python directement sur cette page web, au milieu du cours !

![](images/pythonlearn.jpg){ width=30%; : .center }
```python
print("Hello World")
```

On y trouvera :

* Des rappels de cours ;
* Des éléments auxquels il faut être particulièrement vigilant
* Des exercices à réaliser directement dans les encarts contenant du code (appelés "IDE Python").

## Cette page est en développement permanent

⚠️ N'hésitez pas à signaler tout passage imprécis ou contenant des erreurs.

<table>
<tr>
    <td style="border:1px solid; font-weight:bold;">Thème</td>
    <td style="border:1px solid; font-weight:bold;">Niveau</td>
</tr>
<tr>
    <td style="border:1px solid; font-weight:bold;">Python</td>
    <td style="border:1px solid; font-weight:bold;">Débutant entraîné</td>
</tr>
</table>

<!-- Démo de plusieurs onglets ou panneaux -->

!!! danger "Attention, dans un même cadre il peut y avoir plusieurs onglets !"

    === "Panneau 1"
        Ici du texte concernant ce panneau 1. <br>
        Il peut prendre plusieurs lignes

    === "Panneau 2"
        Là du texte concernant le panneau 2.
        Il peut prendre plusieurs lignes etc

    === "Panneau 3"
        Et voili voilou du texte concernant ce panneau 3.
        Avec plusieurs lignes et cie

<!-- Démo astuce à dérouler -->

???+ question "Exemple d'IDE pour tester du code"

    Compléter le script ci-dessous comme dans un éditeur python :
    pour tester, vous pouvez cliquer sur "play" (1è icône avec flèche vers la droite).
    Ce script peut être réinitiaisé. Votre saisie peut être conservé (bouton nuage "télécharger").


    {{IDE('scripts/print_hello')}}


??? success "Astuce ou Solution - cliquer dessus pour dérouler"
    On peut donner la solution sous forme de code à copier :
    ```python
    # Code de la solution
    ```       

<!-- Démo astuces à cliquer -->

!!! warning inline end "Important"

    Noter l'icône gris clair des carrés en haut à droite (servant à copier le code)
    
    Prenez le temps de lire les commentaires
    en cliquant sur les +


```python
    note = float(input("Saisir votre note : "))
    if note >= 16:  # (1)
        print("TB")
    elif note >= 14:  # (2)
        print("B")
    elif note >= 12:  # (3)
        print("AB")
    elif note >= 10:
        print("reçu")
    else:
        print("refusé")
```

1. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

2. :warning: `elif` signifie **sinon si**.

3. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

<!-- notes de fin -->

On suppose que vous avez déjà été bien initié à python avant de parcourir ce site !
Si nécessaire, vous pouvez ouvrir des tutos<sup>(1)</sup> dans un autre onglet pour vous aider. <br>
Le but est de faire tous les tests demandés sur ce site en prenant le moins de temps possible.
Un compte-rendu propre est à rendre avec les screenshots de chaque résultat.
<br>
<br>
🌐 Exemple de fichier à télécharger :
Fichier `mementopython3` : [Clic droit, puis "Enregistrer la cible du lien sous"](a_telecharger/mementopython3.pdf)
<br>
<br>
[Site d'Edupython](https://edupython.tuxfamily.org/){ .md-button target="_blank" rel="noopener" }


[^1]: Par exemple : [tutoriel général python](https://darchevillepatrick.info/python/python2.php#corps){:target="_blank" }




