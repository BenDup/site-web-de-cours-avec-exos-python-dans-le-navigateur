---
author: BenDup
title: Les instructions conditionnelles
tags:
  - condition
---


Cette notion est l'une des plus importante en programmation. L'idée est de dire que si telle variable a telle valeur alors on fait ceci sinon cela.

![condition](images/condition-recherche.jpg){ width=50%; : .center }

Un **booléen** est une valeur qui peut être soit vraie (true) soit fausse (false). Cette valeur est utilisée pour représenter deux états possibles, généralement "vrai" et "faux", "oui" et "non", "1" et "0", etc. Les booléens sont souvent utilisés dans les algorithmes pour effectuer des tests et prendre des décisions. Par exemple, un programme informatique peut utiliser un booléen pour vérifier si un nombre est pair ou impair. Si le nombre est pair, le booléen sera "vrai", sinon il sera "faux".

Les booléens peuvent également être utilisés dans les expressions conditionnelles. Par exemple, si on veut afficher un message seulement si une certaine condition est remplie, on peut utiliser un booléen pour savoir si la condition est vraie ou fausse. Si le booléen est "vrai", le message sera affiché, sinon il ne le sera pas.

En informatique, les booléens sont souvent utilisés en conjonction avec les **opérateurs de comparaison**, qui permettent de comparer deux valeurs entre elles. Par exemple, l'opérateur "égal à" (==) permet de vérifier si deux valeurs sont égales, et renvoie "vrai" si c'est le cas, "faux" sinon. Il existe d'autres opérateurs de comparaison tels que "différent de" (!=), "supérieur à" (>), "inférieur à" (<), "supérieur ou égal à" (>=), etc. 

### 1. Si - alors

Prenon un exemple, on va donner une valeur à une variable et si cette valeur est supérieur à 5, alors on va incrémenter la valeur de 1 (c-à-d ajouter 1 à cette variable).

```python
a = 10
if a > 5:
  a = a + 1 # Remarquez l'identation, c-à-d le décalage de la ligne qui est NECESSAIRE pour dire le "alors"
print(a)
# Il affiche :
11
```  
!!! info "alors"

    Le 'alors' se dit 'then' en anglais, mais on voit qu'en python il est sous-entendu : "alors a=a+1" s'écrit simplement "a=a+1" avec l'espace de décalage.

Que se passe-t-il si la valeur était inférieure à 5?

```python
a = 3
if a > 5:
  a = a + 1 # Remarquez l'identation, c-à-d le décalage de la ligne qui est NECESSAIRE pour dire le "alors"
print(a)
# Il affiche :
3
```  
On remarque que si la condition n'est pas remplie, les instructions dans la structure conditionnelle sont ignorées. 

### 2. Si - alors - sinon

Il est possible de donner des instructions quelque soit les choix possibles grâce à l'instruction **else**.

???+ question

    Tester le code dans cet IDE :

    {{ IDE('scripts/scripts_if-else') }}

Changeons uniquement la valeur de la variable a : lui affecter la valeur '3' dans l'IDE précédente et exécuter pour voir le résultat.

### 3. Sinon-si

Il est possible d'ajouter autant de conditions précises que l'on souhaite en ajoutant le mot clé elif , contraction de "else" et "if". <br>
Dans l'IDE précédent, tester :

```python
a = 5
if a > 5:
  a = a + 1
elif a == 5:
  a = a + 1000
else:
  a = a - 1
print(a)
```  
Qu'affiche-t-il ? <br>
Dans cet exemple, on a repris le même que les précédent mais nous avons ajouté la condition "Si la valeur est pile égale à 5" que se passe-t-il? <br>
Et bien on ajoute 1000 !

### 4. And-or

Il est possible d'affiner une condition avec les mots clé AND qui signifie " ET " et OR qui signifie " OU ".

On veut par exemple savoir si une valeur est plus grande que 5 mais aussi plus petite que 10 :

```python
v = 15
v > 5 and v < 10
``` 
Il enverra une valeur False

Essayons avec la valeur 7 :

```python
v = 7
v > 5 and v < 10
``` 
Il enverra une valeur True

Pour que le résultat soit TRUE, il faut que les DEUX conditions soient remplies.
<br>
<br>
Testons maintenant la condition OR

```python
v = 11
v > 5 or v > 100
``` 
Il enverra une valeur True

Le résultat est TRUE parce qu' au moins une des deux conditions est respectée.

```python
v = 1
v > 5 or v > 100
``` 
Il enverra une valeur False

Dans ce cas là, aucune condition n'est respectée, le résultat est donc FALSE. 


