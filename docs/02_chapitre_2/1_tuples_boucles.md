---
author: BenDup
title: Tuples et Boucles
---

## I. Listes et Tuples 

**Les listes (ou list/array) en python sont une variable dans laquelle on peut mettre plusieurs variables.**
Vous pouvez ajouter les valeurs que vous voulez lors de la création de la liste python :
```python
liste = [1,2,3]
# Et vous pouvez demander d'afficher la liste :
print(liste)
# Il affiche alors :
[1, 2, 3]
```  

![courses](images/listedecourses.jpg){ width=50%; : .center }

On peut progressivement ajouter des éléments après la création de la liste avec la méthode append (qui signifie "ajouter" en anglais) :
```python
liste = []
liste.append(1)
liste.append("ok")
print(liste)
# Il affiche alors :
[1, 'ok']
```  
On voit qu'il est possible de mélanger dans une même liste des variables de type différent. On peut d'ailleurs mettre une liste dans une liste !

Pour lire une liste, on peut demander à voir l'index de la valeur qui nous intéresse:

```python
liste = ["a","d","m"]
print(liste[0])
# Il affiche alors :
a

print(liste[2])
# Il affiche alors :
m
``` 
Le premier item commence toujours avec l'index 0. Pour lire la premier item on utilise la valeur 0, le deuxième on utilise la valeur 1, etc.
Il est d'ailleurs possible de modifier une valeur avec son index :

```python
liste = ["a","d","m"]
liste[2] = "z"
print(liste)
# Il affiche alors :
['a', 'd', 'z']
```  
Il est parfois nécessaire de supprimer une entrée de la liste. Pour cela vous pouvez utiliser la fonction del .
```python
liste = ["a", "b", "c"]
del liste[1]
print(liste)
# Il affiche alors :
['a', 'c']
```  
Il est possible de compter le nombre d'items d'une liste avec la fonction len .
```python
liste = [1,2,3,5,10]
print(len(liste))
# Il affiche alors :
5
```  

-------------------------------
A partir des types de base (int, float, etc.), il est possible d’en élaborer de nouveaux. On les appelle des types construits.
Un exemple de type construit est le tuple. Il permet de créer une collection ordonnée de plusieurs éléments. En mathématiques, on parle de p-uplet. Par exemple, un quadruplet est constitué de 4 éléments. <br>
**Les tuples ressemblent aux listes, mais on ne peut pas les modifier une fois qu’ils ont été créés.** <br>
On dit qu’un tuple n’est pas mutable. On le définit avec des parenthèses.

```python
a = (3, 4, 7)
print(type(a))
# Il affiche alors :
class 'tuple'
```  

Parfois, les tuples ne sont pas entourés de parenthèses, même s’il s’agit quand même de tuples.

Ainsi, on peut utiliser la notation suivante :

```python
b, c = 5, 6
print(b)
# Il affiche alors :
5

print(c)
# Il affiche alors :
6
```  

### 1. Exemple simple

Dans une liste de courses au supermarché, ajouter du jambon, puis afficher la liste :

???+ question

    Modifier le code dans cet IDE :

    {{ IDE('scripts/scripts_commission') }}


### 2. Exemple plus évolué

Dans une recette qui ne doit pas être modifiée, entrer les éléments suivants : farine, oeufs, beurre, sucre. Afficher le 3è élément.
Copier-coller le code ci-dessous et l'utiliser dans l'IDE précédent :
```python
recette=... # Compléter cette ligne
```    


## II. Boucles

Une boucle ça se répète, se répète, se répète...
![chat](images/chat-robot-aspi.gif){ width=50%; : .center }

**Boucle bornée** <br>
Quand on sait combien de fois doit avoir lieu la répétition, on utilise généralement une boucle for. <br>
**Boucle non bornée** <br>
Si on ne connait pas à l’avance le nombre de répétitions, on choisit une boucle while.

### 1. Exemple simple

Dans l'IDE précédent, tester :
```python
for i in [0, 1, 2, 3]:
    print("i a pour valeur", i)
```  
Puis tester :
```python
for i in range(4):
    print("i a pour valeur", i)
```  
On constate que ce sont deux méthodes différentes d'obtenir la même chose : i vaudra de 0 à 3, donc python répétera 4 fois ce qu'on lui demande.
Pour parcourir les indices d’une liste, il est possible de combiner range() et len() comme ci-dessous :
```python
c = ["Marc", "est", "dans", "le", "jardin"]
for i in range(len(c)):
    print("i vaut", i, "et c[i] vaut", c[i])
```  

### 2. Exemple plus évolué

Le mot-clé while signifie tant que en anglais. Le corps de la boucle (c’est-à-dire le bloc d’instructions indentées) sera répété tant que la condition est vraie.
Dans l’exemple ci-dessus, x sera multiplié par 2 tant que sa valeur reste inférieure à 10.
```python
x = 1
while x < 10:
    print("x a pour valeur", x)
    x = x * 2
print("Fin")
``` 

!!! danger "Attention"

    Si la condition est fausse au départ, le corps de la boucle n’est jamais exécuté. Si la condition reste toujours vraie, alors le corps de la boucle est répété **indéfiniment** (plantage du programme).  

Pour stopper immédiatement une boucle on peut utiliser le mot clé break :
```python
liste = [1,5,10,15,20,25]
for i in liste:
    if i > 15:
            print("On stoppe la boucle")
            break
    print(i)
``` 
On obtient l'affichage : <br>
1 <br>
5 <br>
10 <br>
15 <br>
On stoppe la boucle <br>
<br>
**Dans l'IDE précédent, faire l'exercice suivant :** <br>
Calculez la somme d'une suite de nombres positifs (entrés à la queue leu-leu par un utilisateur). Lui indiquer à la fin combien il y avait de données et combien étaient supérieures à 100. Entrer 0 indique la fin de la suite.

```python
# -*- coding : utf8 -*- 
"""Somme d'entiers et nombre d'entiers supérieurs à 100.""" 
# Programme principal ========================================================= 
somme, nombreTotal, nombreGrands = 0, 0, 0 # Penser à initialiser toutes les valeurs !
  
x = int(input("x (0 pour terminer) ?")) # la 1è valeur est demandée une fois avant d'entamer la boucle (puis sera demandée à la fin de chaque étape)
while x > 0: 
    somme ... # Compléter
    nombreTotal ... # Compléter
    if x > 100: 
        nombreGrands ... # Compléter
    x = int(input("x (0 pour terminer) ?")) 
  
print("\nSomme :", somme)  # (1)
print("Il y avait", nombreTotal, "valeur(s) en tout, dont", ... "supérieure(s) à 100") ... # Compléter
``` 

1. Rem : l'instruction `\n` dans une chaîne sert à sauter une ligne à l'affichage ; c'est ainsi plus lisible par rapport aux éventuels affichages précédents.
