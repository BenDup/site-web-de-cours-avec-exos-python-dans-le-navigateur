---
author: BenDup
title: Les fonctions en python
tags:
  - function
---

Une fonction (ou function) est **une suite d'instructions que l'on peut appeler avec un nom**.

![robot](images/robot-fonctions.jpg){ width=50%; : .center }

Vous pouvez créer une fonction toute simple qui ajoute 10 à une valeur :
```python
def ajout10(a):
    return a + 10
# Lancer le prog. Si à tout moment on tape :
# ajout10(4)
# Alors il affiche :
# 14
```  

Remarquer l'espace avant le mot clé "return", il s'agit d'une indentation, c'est-à-dire un espace qui améliore non seulement la lecture de la fonction mais qui indique que nous sommes toujours dans la fonction. Lorsque l'action demandée n'est plus dans la fonction, il **ne faut plus indenter le texte**.<br>
Pour indenter du texte, vous devez appuyer sur la touche TAB de votre clavier -ou dans d'autres cas créer 4 espaces manuellement-. 
<br>
<br>
La fonction `addition` prend en paramètres deux nombres entiers ou flottants, et renvoie la somme des deux. Entraînez-vous ci-dessous, go !

???+ question "Compléter ci-dessous"

    {{ IDE('scripts/addition') }}
    
!!! info "Paramètres"

    Lorsque vous indiquez des paramètres à une fonction (par exemple a et b), ces dernièrs doivent impérativement être renseignés lorsque vous appelez la fonction, sans quoi une erreur apparaitra.
<br>
<br>
# Aller plus loin

Vous pouvez utiliser l'opérateur splat : * <br>
Par exemple, on peut faire appel à une fonction en fournissant des valeurs via une liste :
```python
def augmente_moi(*param):
    return param[0] + param[1] + param[2]
# Si on tape : 
# augmente_moi(4,10,2.5)
# Il affiche :
# 16.5
```  

L'utilisation de l'étoile permet de passer par une liste. En reprenant la suite de l'exemple précédent :

... data = [1, 2, 3] <br>
... augmente_moi(*data) <br>
<br>Il affiche alors :<br>
6

## Utilisation de splat dictionnaire

!!! info "Dictionnaire"

    Comme on l’a vu avec les listes et les tuples, à partir des types de base (int, float, etc.) il est possible d’élaborer de nouveaux types qu’on appelle des types construits.

    Un nouvel exemple de type construit est le dictionnaire.

    Les éléments d’une liste ou d’un tuple sont ordonnés et on accéde à un élément grâce à sa position en utilisant un numéro qu’on appelle l’indice de l’élément.

    Un dictionnaire en Python va aussi permettre de rassembler des éléments mais ceux-ci seront identifiés par une clé. On peut faire l’analogie avec un dictionnaire de français où on accède à une définition avec un mot.

    Contrairement aux listes qui sont délimitées par des crochets, on utilise des accolades pour les dictionnaires.

    *mon_dictionnaire = {"voiture": "véhicule à quatre roues", "vélo": "véhicule à deux roues"}*

    Un élément a été défini ci-dessus dans le dictionnaire en précisant une clé au moyen d’une chaîne de caractères suivie de ":" puis de la valeur associée

    *clé: valeur*

    On accède à une valeur du dictionnaire en utilisant la clé entourée par des crochets avec la syntaxe suivante :

    *mon_dictionnaire["voiture"]*

    -> il affiche : 'véhicule à quatre roues'

    Il est très facile d’ajouter un élément à un dictionnaire. Il suffit d’affecter une valeur pour la nouvelle clé.

    *mon_dictionnaire["tricycle"] = "véhicule à trois roues"*


Prénons l'exemple de cette fonction:

... def test(firstname="", lastname=""): <br>
...     return "{} {}" .format(firstname, lastname) <br>

Créons notre dictionnaire (un peu comme une liste mais avec des descripteurs) : <br>

... data = {'firstname':'olivier', 'lastname':'engel'}
<br>
<br>
Et envoyons notre variable avec une étoile * (il affichera les descripteurs) <br>

... test(*data)
<br>Il affiche alors :<br>
'firstname lastname'
<br>
<br>
Puis avec deux étoiles ** (il affichera les valeurs) <br>

... test(**data)
<br>Il affiche alors :<br>
'olivier engel'

## Exemple d'exercice avec une fonction à 3 variables

![modif-RVB](images/pyt-modif-rvb.jpg){ width=90% }
<br>
*tiré de "Cahier python" aux éditions Belin*

