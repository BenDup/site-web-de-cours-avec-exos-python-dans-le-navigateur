---
author: Votre nom
title: TP de physique
tags:
  - matplotlib
  - pyplot
  - Difficulté *
---

# TP SPC

![physique](images/savant-fou-phys.png){ width=50%; : .center }

Voici des exemples concrets d'utilisation de python avec de vraies mesures physiques.

!!! danger "Au BAC"

    Python est de plus en plus présent au BAC dans de nombreuses matières/filières (maths spc svt écogestion etc). Mais ce qui est sorti pour l'instant reste très accessible, sous deux formes : <br>
    **&#167;** Soit on vous demande de créer un programme entièrement, mais il sera ultra simple et de quelques lignes seulement ; <br>
    **&#167;** Soit on vous donne un programme plus long/compliqué déjà rédigé, mais avec 3 ou 4 lignes importantes floutées (ou remplacées par des X ou des points etc) et on vous demande de compléter ces endroits. Avec un peu de logique et en connaissant les bases, c'est parfaitement faisable. <br>
    Les exercices de SPC sur cette webpage sont faits de cette manière : un programme déjà prêt, long et avec des fonctions complexes (pour vous faire stresser un coup), mais en réalité les 3-4 lignes à compléter sont vraiment simples.

**Exemple d'exercices de quelques lignes à réaliser en entier** <br>
Dans l'IDE ci-dessous, rédiger un programme python qui : <br>
&#167; En entrée : demande à l'utilisateur la date actuelle (jour, mois, année).  <br>
&#167; En sortie : affiche le nombre de jours écoulés depuis le début de l'an 2000 (on simplifiera -> un mois = 30 jours, un an = 365 jours).

???+ question

    Créer entièrement le code dans cet IDE :

    {{ IDE('scripts/scripts_AFXxxxxxxxxxxxxx') }}


!!! example "Indications"

    === "Tout petit !"
        Faisable proprement en 5 lignes !

    === "Etapes"
        Faire entrer les 3 variables (nombres entiers) ; <br>
        Faire le total des jours depuis 2000 ; <br>
        Afficher proprement le résultat.

    === "Rem"
        Evidemment dans ces 5 lignes on n'anticipe pas de gestion d'erreur avancée (par exemple si l'utilisateur entre "123" pour l'année).


<br>
<br>

## Mécanique

**Lancer de caillou**

Un caillou est lancé. Pendant sa chute en avant, on réalise une chronophotographie (1 photo tous les 1/10è de seconde).
Grâce à une échelle précise, on peut localiser sur chaque photo les coordonnées du caillou.

![caillou](images/lancer-caillou.jpg){ width=30% }

L'objectif est de tracer les vecteurs vitesse du caillou modélisé par un point lors de son mouvement.

!!! info "Vecteurs"

    Pour représenter les vecteurs vitesse, nous allons faire appel à la fonction `quiver` du module `matplotlib.pyplot`. Elle permet de représenter un vecteur de coordonnées (U,V) au point de coordonnées (X,Y) avec la syntaxe `plt.quiver(X, Y, U, V)`.

Bien comprendre les étapes du code proposé, puis le compléter comme demandé. Faire un screenshot du résultat graphique.

<div class="centre">
<iframe 
src=
https://console.basthon.fr/?script=eJydU8tu4jAU3SPxD3fEIkkbMgkU-pCyqGbbxSxmh1BkzKX1jON4bAdBv2j6HfzYXIcE0gppHlk4J7nvc49FqSvjoGROy8pJsUr03iNgFrR0w8FwMHpCC5tKcScqZcf_9fg8a9zAN8M4mmKNttCVFU3GkK0sF9aijSuzrpRCtNHDcAD0UAuJ5cw5NJfc4pKZH2jy4DqIzgFOOIlh8LUrALbmHK0VWxqk4rzWhzdCmhmQCHZv3eFXif0MO8lWKMNgB6ggLKPONoKnw9szqjVSaMAkVarXCK-tWz-BKMM0ubsqhTo3HsVZMrkq2a73K-qo4UzyWhZb4dAbLo7rsNQnbrbFLl8sO7w_4U1lQIFQYJh6xlBiv4Nx1oU3rRqhXKii8x9KmjCtacTwHLVQ19ly3PtcRp_DppejpYXLqJdoBI_fq9p5ohjwimRmmXLoOz06NUH5MfRhnLXNG3S1Ub6NmDyPA3cMGdQGLSrH_FqLLXKHtekYu0hYm6eb2VMj3lNDHtE7Smh7P2uSSk9wC7GM4ZSUvsAnbt5-nsZMKpVY1Eo4mwe7fRBTBYktPlrzbNYS9NfXoL1_tKjq2bCyxGZlXGgm__X6kVjSJE3jNJmlpMPUH4QmHk08mno09ejGoxuPZoRoM6_5Ynb8dz-j466xzumYTCji9paOyZSyzH3Su7lPP6VC6T3FulPdLI0hSRJoqi9JIV9IFvLw1oiELuSWOKqNhXUNp72_E0KxzT_ekl38Gjui6Q_a8G6dFhpO4XGzEfylqQxErX5B-OQtw8Fv_dmKLw
width="1100" height="600" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>


## Electricité

**Tension d'un dipôle résistif en fonction de l'intensité**

![resistance](images/u-egal-ri.png){ width=35% }

Pour modéliser cette caractéristique, il est possible d’utiliser la fonction native `polyfit` de Numpy que vous n'avez pas besoin de connaître, mais qui peut vous être donnée dans un programme pour deviner l'équation de la droite obtenue. La fonction créée à partir de cette fonction native sera du style :

```python
def modelisation(x,y):
    modele=np.polyfit(x,y,1)
    equation=("U = "+format(modele[0],".2E")+"x I +"+format(modele[1],".2E"))
    plt.plot(x,modele[0]*x+modele[1],color='red',label=equation)
    plt.legend()
```

Bien comprendre les étapes du code proposé, puis le compléter comme demandé. Faire un screenshot du résultat graphique.

<div class="centre">
<iframe 
src=
https://console.basthon.fr/?script=eJyVUr1OwzAQ3iv1HY50aEJClBS6IGVgYKgEC6Jdqg5ucmkt_FfbkZq3YYTn6Ithpw0tTGBLp_P9fnefKVdSW-DEKiYto-tUtV4DYkAxOxyMgHYhqEE0XLVgZGOAIQjJQWli6a5BCIQKhgMX_YQGailKS6UwN_85Pr3CGkrZ6DWG-6SN7ocDcGcEz2itRmBjskeoXAuyNiU1xmmm0c5OhUVhqD18AgogXB0-NJpjuhsjbRlZIwuDOYSLKIj6uq_UldV-mo0maovnBFMS62b2MBJO9BvqYhyPE1PkWRb1ULmskFFD_LCXgDs7FkKlSrK2prYrk5_a4q7pMgoPp4AgrqV2-w-PWctslQTp5DGI4mAPM4h_-_PeH53ResZcj-8K1_v4HF1KJh16jdU46dZQ9AguKjDcoKjC6ESiY1a6lXDuNSpKqgj7K4lzPzjRmrRhmqaRW_MLcsWodsxBBod3mC6Gg9k5apklWZrlXky8uPXizovpqgN0-hKzZO6eP5Z-NPlf-lDXtNxekglX3tPfL5E91d0
width="1050" height="600" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>


!!! info "Puissances de 10"

    Les valeurs modélisées par python seront écrites du style "1.54E+07" ce qui signifie 1,54 fois 10 puissance 7.

Pour finir, deviner la valeur de la résistance. Pour ceux qui ont un peu de mal, c'est le coefficient directeur de la droite obtenue.

