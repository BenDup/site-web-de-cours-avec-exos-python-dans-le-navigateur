---
author: Votre nom
title: Le morse
tags:
  - 3-dictionnaires
  - Difficulté **
---

## Le code morse : 

![morse](images/code_Morse.png){ width=40%; : .center }

Crédits : F1jmm, [CC BY-SA 4.0](https:https://creativecommons.org/licenses/by-sa/4.0){:target="_blank" } , Wikimedia Commons


!!! info "Utilisation d'un dictionnaire"

    On représente le code morse à l'aide d'un dictionnaire, on ne s'intéresse qu'aux lettres en majuscules non accentuées.  
    Pour l'espace on utilise le slash (par exemple).
    Vous pourrez recopier dans votre code ce dictionnaire.

    ```python
    morse = {' ': '/', 'E': '°', 'I': '°°', 'S': '°°°', 'H': '°°°°', 'V': '°°°-', 'U': '°°-', 'F': '°°-°',
    'A': '°-', 'R': '°-°', 'L': '°-°°', 'W': '°--', 'P': '°--°', 'J': '°---', 'T': '-', 'N': '-°', 'D': '-°°',
    'B': '-°°°', 'X': '-°°-', 'K': '-°-', 'C': '-°-°', 'Y': '-°--', 'M': '--', 'G': '--°', 'Z': '--°°', 
    'Q': '--°-', 'O': '---'}
    ```

## Travail à faire : 

Ecrire un script qui permet de déchiffrer un message envoyé en morse.

!!! warning "Contraintes"

    * Le script sera bien structuré, avec une ou plusieurs fonctions
    * Vous utiliserez le dictionnaire donné

!!! example "Exemple d'exécution"

    Vous pouvez avoir des noms de fonctions différents. Ceci n'est qu'un exemple.

    ```pycon
    >>> message = 
    '-°°°*°-°*°-*°°°-*---*/°---*°*°°-*-°*°*/°--°*°-*-°°*°-*°--*°-*-°*/°-°°*°-*/-°*°°°*°°*/°*°°°*-*/°-*°°°-*°*-°-°*/-*---*°°*'
    >>> decode_mots(message, morse)
    'BRAVO JEUNE  PADAWAN LA NSI EST AVEC TOI'
    ```

## Solution : 

**Rassurez-vous**, au lycée on ne vous demandera **pas** de choses aussi difficile (sauf si vous choisissez la filière NSI) ! <br>
Mais j'ai choisi cet exemple pour vous montrer ce qu'on peut faire de plus évolué avec quelques lignes de code. <br>

Le script que je vous propose ci-dessous fait les choses suivantes :

1. Il définit le dictionnaire `morse` comme vous l'avez fourni.

2. La fonction `morse_to_text` prend un message en code Morse comme entrée et le décode :

- Elle inverse d'abord le dictionnaire `morse` pour faciliter la recherche.

- Elle sépare le message en mots (en utilisant '/' comme séparateur).

- Pour chaque mot, elle sépare les lettres en Morse et les décode.

- Elle assemble les lettres décodées en mots, puis les mots en une phrase.

3. Enfin, le script teste la fonction avec un message Morse exemple.

```python

morse = {' ': '/', 'E': '°', 'I': '°°', 'S': '°°°', 'H': '°°°°', 'V': '°°°-', 'U': '°°-', 'F': '°°-°',
'A': '°-', 'R': '°-°', 'L': '°-°°', 'W': '°--', 'P': '°--°', 'J': '°---', 'T': '-', 'N': '-°', 'D': '-°°',
'B': '-°°°', 'X': '-°°-', 'K': '-°-', 'C': '-°-°', 'Y': '-°--', 'M': '--', 'G': '--°', 'Z': '--°°', 
'Q': '--°-', 'O': '---'}

def morse_to_text(morse_code):
    # Inverser le dictionnaire pour avoir le code Morse comme clé et la lettre comme valeur
    morse_inverse = {v: k for k, v in morse.items()}
    
    # Séparer le message en mots
    morse_words = morse_code.split('/')
    
    decoded_message = []
    for word in morse_words:
        decoded_word = ''
        # Séparer chaque mot en lettres
        morse_letters = word.split()
        for letter in morse_letters:
            if letter in morse_inverse:
                decoded_word += morse_inverse[letter]
        decoded_message.append(decoded_word)
    
    # Joindre les mots décodés avec des espaces
    return ' '.join(decoded_message)

# Test du programme
morse_message = "°° / °- -- / °- / -°-° --- -°° ° °-°"
decoded = morse_to_text(morse_message)
print(f"Message codé : {morse_message}")
print(f"Message décodé : {decoded}")

```

Vous pouvez exécuter ce script et il décodera le message Morse fourni. Pour utiliser le script avec d'autres messages, il suffit de changer la valeur de `morse_message`.
