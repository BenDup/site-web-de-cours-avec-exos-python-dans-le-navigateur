---
author: BenDup
title: Chapitre 3 - graphiques
tags:
  - graph
---

Python peut appeler des bibliothèques graphiques comme turtle, matplotlib etc.

**Exemple avec turtle** <br>
![](images/image_prete.png){ width=10%; align=right } <br>
Appuyer sur "Exécuter" à gauche, attendre quelques secondes, <br>
 puis cliquer sur l'icône "image" (4è avant la fin, lorsqu'elle clignote) :
<br>

<div class="centre">
<iframe 
src=
https://console.basthon.fr/?script=eJxLK8rPVSgpLSrJSVXIzC3ILypR0OLlyshMSYUIamjyciVnFiUDWYYGBkBOQWpeaQFIND2_JF_D1EAHLpySX56HohxDtS5pyg10THGpNdAxJkG9gY4uWDmSdohASn4e0IcAaadINQ
width="900" height="500" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>

Et avec plus de détails et d'étapes indépendantes (cliquer sur chaque Entrée puis "exécuter" sur chacune, dans l'ordre) :

<div class="centre">
<iframe 
src=
https://notebook.basthon.fr/?ipynb=eJzFVMtu2zAQ_JUFc5EKpfUjvQjwP-TQnqrAYMlVTIRaEnw0NQx_T5Hv8I91GbuxHCBAmjaJTtwdamYwXHIjFFobRfttIwZMUsskRbvZNvf9ZVp7FK0YZLjR7pZEI6LLQZXelyB3vxxFyATRDN4ipGAkXVv8KLbNCZ1IIceEWrS8wEfkymkcE_fBDZBySMzIvC4k-NBRR70LYMAQBBbBal63HQF_3L-VQVezyaTedyz2qZrOSjlIQ9Y5X9UsgT9R5WQcLZXLlERL2dpGuJx8TiWDq0e-nxHD12SsifsckL2QKgIQdncqh2h-IHjeytFIhQG8zdEgAw9ZxVfJSmMPg0umr5RL2ADVcEjL9ECwWMD0T-MQ4Wm0Y3CccWGrT6FR2KVEGxFa-Avuo89PM3YK5zCtX6Le0dnKaNynUdUAZ8AHoaRaIVgJicPJyJuiR9TVpGZ832Nnnn_sqPDDAniQXmlYHu6MNn2_uwtI6X9Nwvi0p_88-C-Wnr2f9Pz9pC_eVvrpCR6b-vy2pp4ncNWcCNxgIH4xPKpSkRwKk1-nlaM5E2oTvZXr5QG4vAegIJavTJbXx-1iy_7oOz8Vg2Tli2OxHAy5INrZ9jff2FnB
width="900" height="900" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>


Bon, nous on va être plus sérieux... Imaginons que nous devons **tracer un graphique en TP de physique (matplotlib)**.


## Prérequis

Il faudra faire appel aux bibliothèques utiles en début de programme (sous-partie "pyplot" venant de matplotlib, et numpy). 
Du coup vous verrez souvent ces programmes commencer par quelque chose du genre :

*... from matplotlib import pyplot as plt (ou import matplotlib.pyplot as plt)* <br>
*... import numpy as np*

Rem : *as plt* veut dire qu'on rebaptise "matplotlib.pyplot" en "plt", ce qui est bien plus court à chaque fois qu'on doit l'appeler !

??? tip "Aller plus loin"

    Il existe bien d'autres bibliothèques comme : <br>
    - `random` qui permet de choisir ou générer des chiffres au hasard ; <br>
    - `folium` qui sert à exploiter des cartes pour faire de la géolocalisation ; <br>
    - `panda` qui permet la gestion de (beaucoup) de données ; <br>
    - `pillow` pour manipuler les pixels d'une photo ; <br>
    - etc.  

## Commandes basiques

Pour créer des listes de données (les mesures du TP de SPC), on peut utiliser des listes classiques :

x = [0,1,2,3,4] <br>
y = [23,31,45,78]

Les deux listes doivent évidemment être de même longueur (eh oui, un point doit toujours avoir une abscisse et une ordonnée) !

!!! info "alternative"

    On peut aussi utiliser des array à une dimension avec numpy : <br>
    x = np.arange(10) <br>
    y = 2*x + 5 


Fermer les objets plt éventuellement existants :

plt.close() 

**Réaliser le plot (c-a-d le tracé) :**

```python
plt.plot(x, y, "-*b", markersize=8, label="nom de la courbe")
```

De nombreuses options sont possibles pour les "marker":

-> Options de ligne : "-" pour ligne continue, "--" pour des tirets <br>
Pour faire un nuage de points (donc sans ligne), n’utilisez pas d’option de ligne, exemple: "+r"

-> Options de marker : *, +, o, ^

-> Options de couleur : b: bleu, k: noir, r: rouge, g: vert...


**Utiliser l’objet plt :**

Ajouter un titre aux axes :

*plt.xlabel("titre des abscisses", fontsize=8)*
*plt.ylabel("titre des ordonnées", fontsize=8)*

Afficher la legende (label de la courbe) :

*plt.legend(loc='best', fontsize=8)*

L’argument loc="best" permet de placer automatiquement la légende au meilleur endroit (c’est à dire là où elle ne va pas chevaucher une courbe)

Afficher le titre du graphique :

*plt.title("titre du graphique", fontsize=8)*

Ajouter du texte sur le graphique :

*plt.text(2,8,"j'ecris ici si je veux !", fontsize=8)*

Le texte sera ajouté aux coordonnées [ x=2 , y=8 ]

Modifier les limites des axes :

*plt.xlim(0,10)*
*plt.ylim(0,25)*

Ou équivalent :

*plt.axis([0,10,0,25])*

Afficher le graphique :

*plt.show()*

Même après avoir affiché le graphique, l’objet `plt` est toujours actif, il est encore possible de modifier le graphique (y compris dans le shell).
Pour voir les modifications apportées, il faut ré-afficher le graphique `plt.show()`

**LET'S GO on s'entraîne !** <br>
Dans l'IDE "basthon" ci-dessous, tracer une droite "y = 5x + 38" avec des petites croix, en ligne rouge.
Le prog est presque entier, il n'y a que 2 lignes à écrire : <br>
??? tip "Astuce"

    On peut créer un tableau des valeurs de "y", ou plus simple : marquer l'équation directement dans le `plot` à la place du "y"

<div class="centre">
<iframe 
src=
https://console.basthon.fr/?script=eJxdjTFuwzAMRXcDvgORDpaBQGi3Lr5C7iDbrCOAEhWRRuyepmvO4YvFgpEO2fj53__fh8RZIc4hreAEYqorf_yC00Ss5Hub1nIVP5HWVV0t0O2oddnFCc3XZ1ueu2cHYkHTQtEfhbYlaay1LWx_MHBItD0U8yuwkOuRzEm9ZoQRBVwvgxdBOZ3hh6OK_8Xuuy19Fw4BM1DjloPlPHKM2wPl1Uc4YRwN8dA1PYo2byWF2bcI_ydnmLJLV3-b8X3xwOXKd7OrJ0QfYnU
width="1000" height="500" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>


## Commandes plus évoluées

**Sauvegarder le graphique :**
..........................................................

*plt.savefig("mon_graphique.png")*

Le format par défaut est .png, mais il est possible de choisir un autre format de sauvegarde :

*plt.savefig("mon_graphique.pdf", format = "pdf")*


**Un graphique avec plusieurs courbes.**
..........................................................

```python
import numpy as np
import matplotlib.pyplot as plt

x = np.arange(10)

plt.close() 

plt.plot(x, 2*x, "-*b", markersize=8, label="nom de la courbe 1")

plt.plot(x, 2*x + 5, "-ok", markersize=8, label="nom de la courbe 2")

plt.plot(x, x**2, "-+r", markersize=8, label="nom de la courbe 3")

plt.xlabel("titre des abscisses", fontsize=8)
plt.ylabel("titre des ordonnées", fontsize=8)
plt.legend(loc='best', fontsize=8)
plt.title("titre du graphique", fontsize=8)
plt.show()
```

-> ON OBTIENT :
![nom image](images/graph_multi_courbes.png){ width=30% }


**Un subplot (plusieurs graphiques sur la même figure)**
..........................................................

```python
import numpy as np
import matplotlib.pyplot as plt

x = np.arange(10)
	
y1 = 2*x
	
y2 = x**2
	
y3 = x**3

y4 = 3*x + 6

y5 = x**0.5


plt.close()

plt.figure(1)

g1 = plt.subplot(221)
plt.plot(x, y1, '-^k', label='courbe1', markersize=5)
plt.title('titre 1', fontsize=10)
plt.xlabel("X",fontsize=10)
plt.ylabel("Y1",fontsize=10)
plt.legend(fontsize=10)

g2 = plt.subplot(222)
plt.plot(x, y2, '-*b', label='courbe2', markersize=5)
plt.title('titre 2', fontsize=10)
plt.xlabel("X",fontsize=10)
plt.ylabel("Y2",fontsize=10)
plt.legend(fontsize=10)

g3 = plt.subplot(223)
plt.plot(x, y3, '--or', label='courbe3', markersize=5)
plt.title('titre 3', fontsize=10)
plt.xlabel("X",fontsize=10)
plt.ylabel("Y3",fontsize=10)
plt.legend(fontsize=10)

g4 = plt.subplot(224)
plt.plot(x, y4, '-+g', label='courbe4', markersize=5)
plt.plot(x, y5, '--^y', label='courbe5', markersize=5)
plt.title('titre 4', fontsize=10)
plt.xlabel("X",fontsize=10)
plt.ylabel("Y4",fontsize=10)
plt.legend(fontsize=10)

plt.tight_layout() 
plt.show()
```

-> ON OBTIENT : 
![nom image](images/plusieurs_graphs.png){ width=40% }
<br>
<br>
**Et il existe d'autres types de graphiques...**
..........................................................

![histogram](images/pyt-histogram.jpg){ width=80% } <br>

