---
author: BenDup
title: Crédits
---
Site réalisé par BenDup - Cité Curie 2023.
Le site est hébergé par la forge de [l'*Association des Enseignants d'Informatique de France*](https://aeif.fr/index.php/accueil/).

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/).
L'exécution de code dans la page est possible grâce à `pyodide` (basé sur `web-assembly`).

😀 Un grand merci à  [Vincent-Xavier Jumel](https://forge.aeif.fr/vincentxavier) et [Vincent Bouillot](https://gitlab.com/bouillotvincent) qui ont produit la technique et les tutos qui m'ont permis de réaliser ce site.


